<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CrudController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
    	$comments = Comment::all();

    	$data = array(
    		'comments'	=> $comments
    	);

    	return view('crud-page')->with($data);
    }

     /**
     * Show the form for creating a new crud.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('crud-new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$validator = $request->validate([
	        'title' 		=> 'required',
	        'comment'		=> 'required',
	        'author_name'	=> 'required'
        ]);

    	$comment = Comment::create([
	                    'title'         =>	$request->input('title'),
	                    'comment'       =>	$request->input('comment'),
	                    'author_name'   =>	$request->input('author_name'),
	                ]);

        $comment->save();

        return redirect('crud');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment = Comment::findOrFail($id);
        
        $data = [
            'comment'	=> $comment
        ];

        return view('crud-edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$comment = Comment::find($id);
        
        $validator = $request->validate([
            'title' 		=> 'required',
            'comment'		=> 'required',
            'author_name'       => 'required'
        ]);
        
        $comment->title         = $request->input('title');
        $comment->comment       = $request->input('comment');
        $comment->author_name   = $request->input('author_name');

        $comment->save();
        
        return redirect('crud');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);

        if ( $comment->id != '' ) {
            $comment->save();
            $comment->delete();

            return redirect('crud');
        }

        return back();
    }
}
