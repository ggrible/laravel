<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="{{ url('/') }}"><i class="fa fa-home fa-fw"></i> Back to Home</a>
            </li>
            <li>
                <a href="{{ url('crud') }}"><i class="fa fa-bar-chart-o fa-fw"></i> CRUD Page</a>
            </li>
            <li>
                <a href="{{ url('web-crawl') }}"><i class="fa fa-bug fa-fw"></i> Web Crawl</a>
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->