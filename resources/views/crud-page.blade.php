@extends('theme.default')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">CRUD (Add, Edit, Delete) - Simple Comment System</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>

<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a class="btn btn-primary" href="{{ URL::to('crud/create') }}">Add New</a>
            </div>
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Comment</th>
                            <th>Author Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($comments as $comment)
                            <tr>
                                <td>{{ $comment->title }}</td>
                                <td>{{ $comment->comment }}</td>
                                <td>{{ $comment->author_name }}</td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <a href="{{ URL::to('crud/' . $comment->id . '/edit') }}" class="btn btn-outline btn-primary">Edit</a>
                                        </div>
                                        <div class="col-md-2 text-left">
                                            {{ Form::open(array('url' => 'crud/' . $comment->id, 'class' => 'inline-block', 'id' => 'delete_'.$comment->id)) }}
                                                {{ Form::hidden('_method', 'DELETE') }}
                                                <input type="submit" class="btn btn-outline btn-danger" value="Delete" />
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
    </script>
@endsection