@extends('theme.default')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">CRUD (Add, Edit, Delete) - Simple Comment System</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>

<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div>
            @if ($errors->any())
                <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                </div><br />
            @endif
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                New Comment
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        {{ Form::open(array('action' => 'CrudController@store', 'method' => 'POST', 'role' => 'form')) }}
                            <div class="form-group">
                                <label>Title</label>
                                <input class="form-control" value="" name="title" />
                            </div>
                            <div class="form-group">
                                <label>Comment/Description</label>
                                <textarea class="form-control" rows="3" name="comment"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Author Name</label>
                                <input class="form-control" value="" name="author_name" />
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Submit" />
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection