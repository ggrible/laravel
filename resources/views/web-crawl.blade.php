@extends('theme.default')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Web Crawl</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>

<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a class="btn btn-primary" href="{{ URL::to('web-crawl/create') }}">Search New</a>
            </div>
            <div class="panel-body">
                
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
    </script>
@endsection